package main

import (
	"os"

	"github.com/fiberweb/apikey/v2"
	"github.com/gofiber/fiber/v2"
)

func main() {

	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("¡Api It's working!")
	})

	app.Use(apikey.New(apikey.Config{Key: os.Getenv("API_KEY")}))

	app.Get("/services", func(c *fiber.Ctx) error {
		services := []string{"service1", "service2", "service3"}
		return c.JSON(services)
	})

	port := os.Getenv("PORT")

	if port == "" {
		port = "3000"
	}

	app.Listen(":" + port)

}
